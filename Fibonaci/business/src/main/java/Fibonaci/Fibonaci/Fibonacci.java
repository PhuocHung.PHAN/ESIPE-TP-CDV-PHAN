package Fibonaci.Fibonaci;

import java.util.Scanner;
import org.apache.log4j.*;

public class Fibonacci {
	
	
	
   

   public static int fibonacci(int n) {
       if (n == 0) {
           return 0;
       } else if (n == 1) {
           return 1;
       } else {
           return fibonacci(n - 1) + fibonacci(n - 2);
       }
   }
}
