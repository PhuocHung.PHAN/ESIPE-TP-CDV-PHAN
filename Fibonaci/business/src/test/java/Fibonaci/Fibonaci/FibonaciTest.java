package Fibonaci.Fibonaci;

import org.junit.Test;
import org.junit.Assert;

public class FibonaciTest {
	
	@Test
	public void fibonaciTest() {
		Assert.assertEquals(1, Fibonaci.fibonacci(1));
		Assert.assertEquals(2, Fibonaci.fibonacci(2));
		Assert.assertEquals(3, Fibonaci.fibonacci(3));
		Assert.assertEquals(4, Fibonaci.fibonacci(4));
		Assert.assertEquals(5, Fibonaci.fibonacci(5));
	}
	
	@Test
	public void fibonaciNegative() {
		Assert.assertEquals(-1, Fibonaci.fibonacci(-1));
		Assert.assertEquals(-2, Fibonaci.fibonacci(-2));
		Assert.assertEquals(-3, Fibonaci.fibonacci(-3));
		Assert.assertEquals(-4, Fibonaci.fibonacci(-4));
		Assert.assertEquals(-5, Fibonaci.fibonacci(-5));
	}
}
