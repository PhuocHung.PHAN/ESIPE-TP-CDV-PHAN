package Fibonaci.Fibonaci;

import java.util.Scanner;
import Fibonaci.Fibonaci.Fibonacci;
import org.apache.log4j.*;

public class Main {
	public static Logger logger = Logger.getLogger(Main.class);
	public static void main(String[] args) {
	       Scanner s = new Scanner(System.in);
	       System.out.print("Enter the value of n: ");
	       int n = s.nextInt();
	       if(n==0) {
	    	   n=5;
	       }
	       if(s == null) {
	    	   n=5;
	       }
	       for (int i = 0; i <= n; i++) {
	           logger.info(Fibonacci.fibonacci(i) + " ");
	       }
	   }
}
